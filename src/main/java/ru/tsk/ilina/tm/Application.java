package ru.tsk.ilina.tm;

import ru.tsk.ilina.tm.constant.TerminalConst;

import java.util.Scanner;


public class Application {

    public static void main(String[] args) {
        System.out.println("**WELCOME TO TASK MANAGER**");
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND: ");
            String command = scanner.nextLine();
            parseArg(command);
        }
    }


    public static void exit() {
        System.exit(0);
    }

    public static void parseArg(final String arg) {
        if (TerminalConst.ABOUT.equals(arg)) showAbout();
        if (TerminalConst.VERSION.equals(arg)) showVersion();
        if (TerminalConst.HELP.equals(arg)) showHelp();
        if (TerminalConst.EXIT.equals(arg)) exit();
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Elena Ilina");
        System.out.println("E-MAIL: eilina@tsconsulting.com");
    }

    public static void showVersion() {
        System.out.println("1.0.0");
    }

    public static void showHelp() {
        System.out.println(TerminalConst.VERSION + " - Display program version.");
        System.out.println(TerminalConst.ABOUT + " - Display developer info.");
        System.out.println(TerminalConst.HELP + " - Display list of commands.");
        System.out.println(TerminalConst.EXIT + " - Close application.");
    }


}